#!/bin/bash


# Second pass has not been tested yet...

if [ -z "$1" ]; then
	pass_num=-1
else
	pass_num=$1
fi

if [ -z "$2" ]; then
	sub_pass=0
else
	sub_pass=$2
fi

current_pass="${pass_num}:${sub_pass}"

# Configuration
# -------------
REPO="/media/kuka/579F-1532/Xenomai"
INSTALL_DESTINATION="/home/kuka"
CONC=7
MSG_TMP="/tmp/xenomai_install_msg"

LINUX_SRC="linux-3.5.7"
EXTRA_NAME="fhts-patch"
CONFIG="config-3.5.7-x2621-p6-fhts"

#EXTRA_GIT_PATCH="patch6.diff"

IPIPE_ARCH="x86"
IPIPE_PATCH="ipipe-core-3.5.7-x86-4.patch"

XENOMAI_SRC="xenomai-2.6.3"
XENOMAI_CONFIGURE_OPTIONS="--enable-smp --prefix=/usr/xenomai-${EXTRA_NAME}"
RTNET_SRC="rtnet-0.9.13"

function report ()
{
	if [ $? -ne 0 ]; then
		echo "[FAIL | $1]: Operation '$2' failed!"
		echo "[INFO]: Leaving pass ${current_pass}"
		exit 1;
	else
		echo "[DONE | $1]: Operation '$2' succeeded!"
	fi
}

function message ()
{
	echo "[$1]: $2"
}

function run_config ()
{
	# arg1: make command
	# arg2: name of source package to configure
	echo "[interactive]: Now YOU have to configure $2"
	key="n"

	while [[ "${key}" != "y" && "${key}" != "a" ]]; do
		make "$1"
		echo "[REQS | interactive]: Are you finished configuring?"
		echo ""
		echo "        - 'y' ... yes, continue!"
		echo "        - 'n' ... no, run again!"
		echo "        - 'a' ... abort"
		read -n 1 key
	done

	if [ "${key}" == "a" ]; then
		echo "[interactive]: Aborting due to user request!"
		echo "[INFO]: Leaving pass ${current_pass}"
		exit 0
	fi
}

function report_tool ()
{
	if [ $? -ne 0 ]; then
		echo "[FAIL | tool-check]: Couln't find tool '$1'"
		echo "[INFO]: Leaving pass ${current_pass}"
		exit 1;
	else
		line=`head -n 1 ${MSG_TMP}`
		echo "[DONE | tool-check]: Found tool: '${line}'"
	fi
}

function debian_assert_installed ()
{
	ret=1
	echo ""
	while [ ${ret} -ne 0 ]; do
		dpkg-query -W -f='${Status}' "$1" | grep -E ".* installed" > /dev/null
		ret=$?

		if [ ${ret} -ne 0 ]; then
			message "FAIL | debian" "Couldn't find package '$1'"
			message "REQS | interactive" "You have to install package '$1' manually!"
			message "REQS | interactive" "Press any key when ready (or C-c to abort)"
			read -n 1
		else
			message "DONE | debian" "Found package '$1'"
		fi
	done
}

function tool_check ()
{
	echo "tmp - exists" > ${MSG_TMP}
	report "tool-check" "Check temporary message buffer file"

	head --version > ${MSG_TMP}
	report_tool "head"

	mkdir --version  > ${MSG_TMP}
	report_tool "mkdir"

	cp --version > ${MSG_TMP}
	report_tool "cp"

	tar --version > ${MSG_TMP}
	report_tool "tar"

	git --version > ${MSG_TMP}
	report_tool "git"

	make --version > ${MSG_TMP}
	report_tool "make"

	fakeroot --version > ${MSG_TMP}
	report_tool "fakeroot"

	sudo --version > ${MSG_TMP}
	report_tool "sudo"

	echo ""
	message "DONE | tool-check" "All automated tool checks passed!"
	echo ""

	packages_verified=0

	dpkg-query --version > ${MSG_TMP}
	if [ $? -eq 0 ]; then
		# This is debian based
		# try to check packages
		echo "" > /dev/null
		report_tool "dpkg-query"

		debian_assert_installed "libncurses*dev"
		packages_verified=1
	fi

	if [ ${packages_verified} -eq 0 ]; then
		message "REQS | interactive" "Now please verify that following modules are installed:"
		echo ""
		echo "        * ncurses library"
		echo ""
		echo " -- Press any key to continue (or C-c to abort) --"
		read -n 1
	fi
}

function pass0 ()
{
	current_pass="0:"

	# Prepare installation
	# --------------------

	mkdir -p "${install_dir}"
	report "prepare" "create directory"

	cd "${install_dir}"
	report "prepare" "change to install directory"

	cp "${REPO}/${CONFIG}" "./${CONFIG}"
	report "prepare" "copy default config"

	if [ "${EXTRA_GIT_PATCH}" != "" ]; then
		cp "${REPO}/${EXTRA_GIT_PATCH}" "./${EXTRA_GIT_PATCH}"
		report "prepare" "copy patch"
	fi


	# Copy and extract sources
	# ------------------------
	tar xzf "${REPO}/${RTNET_SRC}.tar.gz"
	report "extract" "Extract RTnet source package"

	tar xjf "${REPO}/${XENOMAI_SRC}.tar.bz2"
	report "extract" "Extract Xenomai source package"

	tar xjf "${REPO}/${LINUX_SRC}.tar.bz2"
	report "extract" "Extract Linux source package"

	message "DONE | status" "Finished pass 0!"
	message "REQS | interactive" "Press any key (or wait 10 seconds) to continue with pass 1 (or C-c  to abort)"
	read -n 1 -t 10
	pass_num=1
}

function pass1 ()
{
	# Patch kernel
	# ------------

	if [ ${sub_pass} -le 0 ]; then
		current_pass="1:0"

		cd "${xenomai_dir}"
		report "patch" "Change to Xenomai sources"

		message "REQS | interactive" "Choose architecture:"
		./scripts/prepare-kernel.sh "--linux=${linux_dir}" "--ipipe=./ksrc/arch/${IPIPE_ARCH}/patches/${IPIPE_PATCH}"
		report "patch" "Apply Xenomai patch"
	fi

	if [[ ${sub_pass} -le 1 && "${EXTRA_GIT_PATCH}" != "" ]]; then
		current_pass="1:1"

		cd "${linux_dir}"
		report "patch-extra" "Change to Linux sources"

		cat "${extra_git_patch}" | git apply
		report "patch-extra" "Apply extra patch"
	fi

	# Config kernel
	# -------------

	if [ ${sub_pass} -le 2 ]; then
		current_pass="1:2"

		cd "${linux_dir}"
		report "config" "Change to Linux sources"

		cp "${config_file}" ".config"
		report "config" "Copy default configuration to Linux sources"

		run_config menuconfig "Linux Kernel"
	fi

	if [ ${sub_pass} -le 3 ]; then
		current_pass="1:3"

		cd "${linux_dir}"
		report "compile" "Change to Linux sources"

		export CONCURRENCY_LEVEL=${CONC}

		make-kpkg clean
		report "compile" "Clean up Linux source package"

		message "REQS | interactive" "Press any key (or wait 10 seconds) to start compilation the Linux Kernel"
		read -n 1 -t 10

		time fakeroot make-kpkg --initrd "--append-to-version=-${EXTRA_NAME}" kernel-image kernel-headers modules
		report "compile" "Compiling the Kernel"

		message "DONE | status" "Finished pass 1!"
		message "REQS | interactive" "Please restart and execute this script in pass 2"
	else
		message "FAIL | install" "sub-pass is too high! (${sub_pass} > 3)"
	fi
}

function pass2 ()
{
	# Check setup
	# -----------
	current_pass="2:0"

	echo ""
	echo "  You made it to the second pass... ;)"
	echo "  Check your current kernel and installation directory and break (C-C) if they are wrong"
	echo "   * Kernel:                  `uname -r`"
	echo "   * Installation directory:   ${install_dir}"
	echo ""

	message "REQS | interactive" "Press any key to continue (or C-c to abort)"
	read -n 1

	# Configure, compile and install Xenomai
	# --------------------------------------
	if [ ${sub_pass} -le 0 ]; then
		current_pass="2:0"

		cd "${xenomai_dir}"
		report "xenomai" "Change to Xenomai sources"

	
		./configure ${XENOMAI_CONFIGURE_OPTIONS}
		report "xenomai" "Configure Xenomai"
	fi

	if [ ${sub_pass} -le 1 ]; then
		current_pass="2:1"

		cd "${xenomai_dir}"
		report "xenomai" "Change to Xenomai sources"

		message "REQS | interactive" "Press any key (or wait 10 seconds) to start compiling Xenomai"
		read -n 1 -t 10

		make -j
		report "xenomai" "Compile Xenomai"
	fi

	if [ ${sub_pass} -le 2 ]; then
		current_pass="2:2"

		cd "${xenomai_dir}"
		report "xenomai" "Change to Xenomai sources"

		message "REQS | interactive" "Install Xenomai"

		sudo make install
		report "xenomai" "Install Xenomai"
	fi

	# Configure compile and install RTnet
	# -----------------------------------
	if [ ${sub_pass} -le 3 ]; then
		current_pass="2:3"

		cd "${rtnet_dir}"
		report "RTnet" "Change to RTnet sources"

		run_config menuconfig "RTnet"
	fi

	if [ ${sub_pass} -le 4 ]; then
		current_pass="2:4"

		cd "${rtnet_dir}"
		report "RTnet" "Change to RTnet sources"

		message "NOTE | interactive" "Press any key or wait 10 seconds to start compiling RTnet"
		read -n 1 -t 10

		make -j
		report "RTnet" "Compile RTnet"
	fi

	if [ ${sub_pass} -le 5 ]; then
		current_pass="2:5"
		
		cd "${rtnet_dir}"
		report "RTnet" "Change to RTnet sources"

		message "REQS | interactive" "Installing RTnet"

		sudo make install
		report "RTnet" "Install RTnet"

		message "DONE | install" "Finished pass 2! System is now ready to use..."
	else
		message "FAIL | install" "sub-pass is too high! (${sub_pass} > 5)"
	fi
}

# Set some paths
# --------------

install_dir="${INSTALL_DESTINATION}/${LINUX_SRC}-${EXTRA_NAME}-package"

xenomai_dir="${install_dir}/${XENOMAI_SRC}"
rtnet_dir="${install_dir}/${RTNET_SRC}"
linux_dir="${install_dir}/${LINUX_SRC}"
config_file="${install_dir}/${CONFIG}"

if [ "${EXTRA_GIT_PATCH}" != "" ]; then
	extra_git_patch="${install_dir}/${EXTRA_GIT_PATCH}"
fi

tool_check


if [ ${pass_num} -eq 0 ]; then

	pass0
	sub_pass=0
fi

if [ ${pass_num} -eq 1 ]; then

	pass1

elif [ ${pass_num} -eq 2 ]; then

	pass2

else
	echo "[error]: Please specify the current pass (0/1 for first pass, 2 for second pass)"
	echo "         0 ... copies the package and then runs pass 1"
	echo "         1 ... configures, compiles and installs the Kernel"
	echo "         2 ... configures and installs Xenomai and RTnet (after rebooting into the new Kernel)"
fi
